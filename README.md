# Requirements
- java 1.8
- maven 3
- ActiveMQ messaging server. Latest release could be downloaded here: [http://activemq.apache.org/download.html](http://activemq.apache.org/download.html)

# Configuration
Change "activemq.tcp" property in public-app/etc/catalinaHome/confpublic-app.properties and secure-app/etc/catalinaHome/conf/secure-app.properties if you are running ActiveMQ on remote server

# Running APP
1. Extract 'apache-activemq-5.11.1-bin.tar.gz' to some directory
2. Start ActiveMQ server by running `<activemq_home>/bin/activemq start`
3. In project root run "mvn clean compile install"
4. To start secure-app: in "secure-app" directory run `mvn -P tomcat clean compile tomcat7:run` - starts embedded tomcat instance on 8080 port
5. To start public-app: in "public-app" directory run `mvn -P tomcat clean compile tomcat7:run` - starts embedded tomcat instance on 8081 port

# Some Description
* Some of REST method invoking examples for JIdea you can find in "jideaREST" directory
* ActiveMQ web manager: [http://localhost:8161/admin](http://localhost:8161/admin)

## public-app REST methods
1. POST http://localhost:8080/public-app/ws/cardInfo (Content-Type: application/json)
request JSON example:
`{
  "info_request": {
  "card_details": [
  {
  "card_number": "12345678910"
  },
  {
  "card_number": "9876543210"
  }
  ]
  }
 }`

## secure-app REST methods
1. GET http://localhost:8081/secure-app/ws/card/{cardNumber} - getting card JSON object
2. POST http://localhost:8081/secure-app/ws/card/ (Content-Type: application/json) - insert card object
3. POST http://localhost:8081/secure-app/ws/card/{cardNumber} (Content-Type: application/json) - update card object 
4. DELETE http://localhost:8081/secure-app/ws/card/{cardNumber} (Content-Type: application/json) - delete card by "cardNumber"

## Objects
Card JSON object example:
`{"cardNumber":"12345","name":"Deniss","surname":"Random"}`
